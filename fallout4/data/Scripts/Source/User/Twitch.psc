Scriptname Twitch extends Quest

;Don't touch this block of properties, doing so will break everything.
Group Mandatory_Properties
GlobalVariable Property Twitch_FormID auto Mandatory
GlobalVariable Property Twitch_SpawnAmount auto Mandatory
GlobalVariable Property Twitch_InteriorAllowed auto Mandatory
Actor Property PlayerRef auto Mandatory 
EndGroup

;Feel free to modify this block however.
Group Custom_Event_Properties
ActorValue Property SpeedMult auto Mandatory
EndGroup

;Leave this import, handles point return system if NPC spawn fails.
Import TwitchWriteCommandReturn
Import TwitchPluginDependency

;--CORE FUNCTIONS---------------------------------------------------------------------------------
Function SpawnNPC()	
	ObjectReference PlayerRefObj = Game.GetPlayer()

	if(Twitch_InteriorAllowed.GetValueInt() == 0) ;0 = Not Allowed, 1 = Allowed
		if(PlayerRefObj.IsInInterior())
			Debug.Notification("Spawn failed, NPC spawn not allowed in interior cells.")
			TwitchWriteCommandReturn()
			return
		endIf
	endIf

	ActorBase thisNPC 

	thisNPC = Game.GetFormFromFile(Twitch_FormID.GetValueInt(), TwitchPluginDependency()) as ActorBase

	;Creates a random distance from the player that the NPC(s) will spawn in.
	float RandomDistance = Utility.RandomFloat(-756, 756)
	
	int spawnedAmount = 0

	;Lets the streamer know something has spawned
	if(Twitch_SpawnAmount.GetValueInt() == 1) 
		Debug.Notification("Someone just spawned something...")
	Else
		Debug.Notification("Someone just spawned some things...")
	endIf
	
	if(thisNPC != None)
		While (spawnedAmount < Twitch_SpawnAmount.GetValueInt())
			spawnedAmount = spawnedAmount + 1
			ObjectReference kspawnedNPC = PlayerRefObj.PlaceAtMe(thisNPC, 1, false, true, true)
			Float fPlayerAngle = PlayerRef.GetAngleZ()
			Float fDistanceInFront = RandomDistance 
			Float fX = Math.sin(fPlayerAngle) * fDistanceInFront
			Float fY = Math.cos(fPlayerAngle) * fDistanceInFront
			kspawnedNPC.MoveTo(PlayerRef, fX, fY)
			kspawnedNPC.Enable()
			if(kspawnedNPC as Actor) 
				kspawnedNPC.MoveToNearestNavmeshLocation()
			endIf
		EndWhile
	endIf	
EndFunction

Function GiveItem()
	PlayerRef.AddItem(Game.GetFormFromFile(Twitch_FormID.GetValueInt(), TwitchPluginDependency()), Twitch_SpawnAmount.GetValueInt())
EndFunction	

Function SetWeather()
	Weather thisWeather = Game.GetFormFromFile(Twitch_FormID.GetValueInt(), TwitchPluginDependency()) as Weather
	thisWeather.ForceActive()
	Debug.Notification("Someone just changed the weather!")
EndFunction	

;--USER DEFINED FUNCTIONS-------------------------------------------------------------------------

Function SlowPlayer()
	Debug.Notification("Your movement speed has been slowed down!")
	PlayerRef.SetValue(SpeedMult, 75)
	Utility.Wait(30)
	PlayerRef.SetValue(SpeedMult, 100)
	Debug.Notification("Your movement speed is back to normal!")
EndFunction	

Function Unequip()
	PlayerRef.UnequipAll()
	PlayerRef.EquipItem(Game.GetForm(0x00021b3b), false, true) ;Add back the Pip-Boy, as it's unequiped by the "UnequipAll()" function.
EndFunction	

;--EVENTS---------------------------------------------------------------------------------------

Event OnStageSet(int StageId, int ItemID)
	if(StageID == 0)
		Debug.Notification("Twitch Integration ready!") ;Initialization
	elseIf(StageID == 10)
		SpawnNPC() ;Spawn NPC function
	elseIf(StageId == 15) 
		GiveItem() ;Give Item function
	ElseIf (StageId == 20)
		SetWeather() ;Handle weather change
	elseIf(StageId == 50)
		SlowPlayer() ;Slows player to 75% of default movement speed for 30s.
	elseIf(StageId == 55)
		Unequip() ;Unequips all items from the player.	
	EndIf
EndEvent
	
