#ifndef _Plugin_Console_H
#define _Plugin_Console_H

#include "../skse/GameMenus.h"

#include <time.h>
#include <fstream>

#define CONSOLE_PLUGIN_VERSION 3

IMenu * MenuManager::GetMenu(BSFixedString * menuName)
{
	if (!menuName->data)
		return NULL;

	MenuTableItem * item = menuTable.Find(menuName);

	if (!item)
		return NULL;

	IMenu * menu = item->menuInstance;
	if (!menu)
		return NULL;

	const int incRef = 0x93F050;
	_asm
	{
		pushad
			pushfd
			mov ecx, menu
			call incRef
			popfd
			popad
	}

	return menu;
}

struct ConsolePointerHolder {
	ConsolePointerHolder() {
		static BSFixedString* consoleString = NULL;
		if (consoleString == NULL) {
			consoleString = new BSFixedString("Console");
		}
		_consolePointer = (int)MenuManager::GetSingleton()->GetMenu(consoleString);
	}

	~ConsolePointerHolder() {
		if (_consolePointer != 0) {
			const int decReference = 0x9241A0;
			int linePointer = _consolePointer;
			_consolePointer = 0;
			_asm {
				pushad
					pushfd
					mov ecx, linePointer
					call decReference
					popfd
					popad
			}
		}
	}

	int GetConsolePointer() { return _consolePointer; }

private:
	int _consolePointer;
};

struct ConsoleFunctions {
	static void ExecuteCommand_Internal(const char * text) {
		ConsolePointerHolder consolePointerHolder;

		int pointer = consolePointerHolder.GetConsolePointer();
		if (pointer == 0) {
			//Something went really wrong.
			return;
		}

		int * unknownStructure = (int*)malloc(64);
		memset(unknownStructure, 0, 64);

		unknownStructure[4] = pointer;
		unknownStructure[6] = (int)(&unknownStructure[7]); // Might work, not sure.
		unknownStructure[9] = (int)text;

		const int runScript = 0x847080;
		_asm
		{
			pushad
				pushfd
				mov eax, unknownStructure
				push eax
				call runScript
				add esp, 4
				popfd
				popad
		}

		free(unknownStructure);
	}
};

#endif
