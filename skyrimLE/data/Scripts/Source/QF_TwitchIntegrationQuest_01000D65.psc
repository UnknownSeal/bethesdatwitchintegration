;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 22
Scriptname QF_TwitchIntegrationQuest_01000D65 Extends Quest Hidden

;BEGIN FRAGMENT Fragment_12
Function Fragment_12()
;BEGIN AUTOCAST TYPE Twitch
Quest __temp = self as Quest
Twitch kmyQuest = __temp as Twitch
;END AUTOCAST
;BEGIN CODE
kmyQuest.SpawnNPC()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_13
Function Fragment_13()
;BEGIN AUTOCAST TYPE Twitch
Quest __temp = self as Quest
Twitch kmyQuest = __temp as Twitch
;END AUTOCAST
;BEGIN CODE
kmyQuest.Unequip()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_6
Function Fragment_6()
;BEGIN CODE
Debug.Notification("Twitch Integration Enabled!")
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_18
Function Fragment_18()
;BEGIN AUTOCAST TYPE Twitch
Quest __temp = self as Quest
Twitch kmyQuest = __temp as Twitch
;END AUTOCAST
;BEGIN CODE
kmyQuest.GiveItem()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_20
Function Fragment_20()
;BEGIN AUTOCAST TYPE Twitch
Quest __temp = self as Quest
Twitch kmyQuest = __temp as Twitch
;END AUTOCAST
;BEGIN CODE
kmyQuest.SetWeather()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_15
Function Fragment_15()
;BEGIN AUTOCAST TYPE Twitch
Quest __temp = self as Quest
Twitch kmyQuest = __temp as Twitch
;END AUTOCAST
;BEGIN CODE
kmyQuest.SlowPlayer()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
