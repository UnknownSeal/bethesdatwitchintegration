package org.bethesdaTwitchIntegration

import bethesdaTwitchIntegration.tiltify.TiltifyVariant
import bethesdaTwitchIntegration.commands.CommandHandler
import bethesdaTwitchIntegration.json.PointLogic
import bethesdaTwitchIntegration.json.SettingsParser
import bethesdaTwitchIntegration.json.SettingsParser.bitMultiplier
import bethesdaTwitchIntegration.json.SettingsParser.campaignID
import bethesdaTwitchIntegration.json.SettingsParser.channelToJoin
import bethesdaTwitchIntegration.json.SettingsParser.pointsWhenDisabled
import bethesdaTwitchIntegration.json.SettingsParser.sharedPrefix
import bethesdaTwitchIntegration.json.SettingsParser.subscriptionPoints
import bethesdaTwitchIntegration.json.SettingsParser.systemEnabled
import bethesdaTwitchIntegration.json.SettingsParser.tiltifyMode
import bethesdaTwitchIntegration.json.TimedLogic
import bethesdaTwitchIntegration.misc.DirectoryMonitoring
import bethesdaTwitchIntegration.misc.MiscLogic

import com.github.philippheuer.credentialmanager.domain.OAuth2Credential

import com.github.twitch4j.TwitchClient
import com.github.twitch4j.TwitchClientBuilder
import com.github.twitch4j.chat.events.channel.ChannelMessageEvent
import com.github.twitch4j.chat.events.channel.CheerEvent
import com.github.twitch4j.chat.events.channel.GiftSubscriptionsEvent
import com.github.twitch4j.chat.events.channel.SubscriptionEvent

import java.util.TimerTask
import java.util.Locale
import java.util.Timer

class BethesdaTwitchIntegration {
    var twitchCredential = OAuth2Credential(
        "twitch", "oauth:3zackwnwgfezvpezcuiti1az6reh5y"
    )

    init {
        SettingsParser.settingsParser()
        val directoryMonitoring = DirectoryMonitoring()
        directoryMonitoring.start()
        MiscLogic.startCleanup()

        if(tiltifyMode) {
            println("[BTI] Connected to campaign $campaignID")
            TiltifyVariant.tiltifyLoop()
        } else {
            println("[BTI] Connected to channel $channelToJoin.")

            twitchClient = TwitchClientBuilder.builder()
                .withEnableChat(true)
                .withChatAccount(twitchCredential)
                .withEnableTMI(true)
                .build()
            twitchClient?.chat?.joinChannel(channelToJoin)

            twitchClient?.eventManager?.onEvent(CheerEvent::class.java) { e: CheerEvent ->
                if (!PointLogic.userExists(e.user.name.lowercase(Locale.getDefault()))) {
                    PointLogic.addUser(e.user.name.lowercase(Locale.getDefault()))
                }
                PointLogic.modifyUserPoints((e.bits * bitMultiplier).toInt(), e.user.name.lowercase(Locale.getDefault()))
            }

            /*
             * Handles the subscription event.
             *
             * Points get multiplied by tier level (1, 2, 3).
             *
             * Only fires if it's not a gifted subscription.
             */
            twitchClient?.eventManager?.onEvent(
                SubscriptionEvent::class.java
            ) { e: SubscriptionEvent ->
                if (!e.gifted) {
                    var levelMultiplier = 0
                    when (e.subscriptionPlan) {
                        "1000" -> levelMultiplier = 1
                        "2000" -> levelMultiplier = 2
                        "3000" -> levelMultiplier = 3
                    }
                    if (!PointLogic.userExists(e.user.name.lowercase(Locale.getDefault()))) {
                        PointLogic.addUser(e.user.name.lowercase(Locale.getDefault()))
                    }
                    PointLogic.modifyUserPoints(subscriptionPoints.toInt() * levelMultiplier,
                        e.user.name.lowercase(Locale.getDefault())
                    )
                }
            }

            /*
            * Handles the gift sub event.
            *
            * Points get multiplied by tier level (1, 2, 3) and then by sub amount.
            */
            twitchClient?.eventManager?.onEvent(GiftSubscriptionsEvent::class.java) { e: GiftSubscriptionsEvent ->
                var levelMultiplier = 0
                when (e.subscriptionPlan) {
                    "1000" -> levelMultiplier = 1
                    "2000" -> levelMultiplier = 2
                    "3000" -> levelMultiplier = 3
                }
                if (!PointLogic.userExists(e.user.name.lowercase(Locale.getDefault()))) {
                    PointLogic.addUser(e.user.name.lowercase(Locale.getDefault()))
                }
                PointLogic.modifyUserPoints(
                    subscriptionPoints.toInt() * levelMultiplier, e.user.name.lowercase(Locale.getDefault())
                )
            }

            twitchClient?.eventManager?.onEvent(
                ChannelMessageEvent::class.java
            ) { e: ChannelMessageEvent ->
                if (e.message.substring(0, 1).equals(sharedPrefix, ignoreCase = true)) {
                    val commandString = e.message.split(" ").toTypedArray()
                    CommandHandler.handleCommand(
                        e.user.name,
                        commandString,
                        twitchClient
                    )
                }
            }
            val chatters = twitchClient?.messagingInterface?.getChatters(channelToJoin)?.execute()
            chatters?.allViewers?.let {
                TimedLogic.userLogger(it)
            }
            timedLoop(twitchClient)
        }
    }

    companion object {

        /**
         * Twitch4J Connection
         */
        private var twitchClient : TwitchClient? = null

        /*
        * A few message functions for ease-of-life.
        */
        fun sendMessage(message : String?) {
            twitchClient!!.chat.sendMessage(channelToJoin, message)
        }

        fun sendInvalidCommand(username : String) {
            twitchClient!!.chat.sendMessage(channelToJoin, "Invalid command @$username")
        }

        fun sendMissingPoints(username : String, cost : Int) {
            twitchClient!!.chat.sendMessage(channelToJoin, "You need $cost to do that, you have ${PointLogic.userPoints(username)} @$username")
        }

        /*
         * Main loop, checks
         */
        fun timedLoop(twitchClient : TwitchClient?) {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (systemEnabled || pointsWhenDisabled) {
                        val chatters = twitchClient!!.messagingInterface.getChatters(channelToJoin).execute()
                        TimedLogic.defaultPointLoop(chatters.allViewers)
                    }
                }
            }, 0, (60 * 1000).toLong())
        }

        @JvmStatic
        fun main(args : Array<String>) {
            BethesdaTwitchIntegration()
        }
    }
}