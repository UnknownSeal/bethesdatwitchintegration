package bethesdaTwitchIntegration.json

import bethesdaTwitchIntegration.json.SettingsParser.pointsPerMinute

object TimedLogic {

    fun defaultPointLoop(users : List<String?>) {
        for (userToModify in users) {
            PointLogic.modifyUserPoints(pointsPerMinute.toInt(), userToModify!!)
        }
    }

    fun userLogger(users : List<String?>) {
        for (userToAdd in users) {
            if(!PointLogic.userExists(userToAdd!!)) {
                PointLogic.addUser(userToAdd)
            }
        }
    }

}