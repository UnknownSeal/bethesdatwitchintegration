package bethesdaTwitchIntegration.commands

import bethesdaTwitchIntegration.json.PointLogic
import bethesdaTwitchIntegration.json.SettingsParser
import bethesdaTwitchIntegration.json.SettingsParser.channelToJoin
import bethesdaTwitchIntegration.json.SettingsParser.commandDelayMillis
import bethesdaTwitchIntegration.json.SettingsParser.lastCommandGlobal
import bethesdaTwitchIntegration.json.SettingsParser.lastDefaultCommand
import bethesdaTwitchIntegration.json.SettingsParser.lastSpecialCommand
import bethesdaTwitchIntegration.json.SettingsParser.randomspawncost
import bethesdaTwitchIntegration.json.SettingsParser.specialCommandDelayMillis
import bethesdaTwitchIntegration.json.SettingsParser.systemEnabled
import bethesdaTwitchIntegration.misc.MiscLogic
import org.bethesdaTwitchIntegration.BethesdaTwitchIntegration

import com.github.twitch4j.TwitchClient

object CommandHandler {

    fun handleCommand(username: String, command: Array<String>, twitchClient: TwitchClient?) {
        val currentTimeMillis = System.currentTimeMillis()
        val commandToValidate = command[0].toLowerCase().substring(1)

        //Checks with the impacted commands if the overhead system is flagged as enabled.
        if (!systemEnabled) {
            when (commandToValidate) {
                "spawn", "misc", "give", "weather", "randomspawn" -> {
                    BethesdaTwitchIntegration.sendMessage("Twitch Integration is not currently enabled.")
                    return
                }
            }
        }

        /*
         * Low-Delay Command Time Verification
         */
        if (currentTimeMillis <= lastDefaultCommand + commandDelayMillis || currentTimeMillis <= lastCommandGlobal + 250) {
            when (commandToValidate) {
                "spawn", "give", "randomspawn" -> {
                    BethesdaTwitchIntegration.sendMessage("Too many commands, please wait a bit.")
                    return
                }
            }
        }

        /*
         * High-Delay Command Time Verification
         */
        if (currentTimeMillis <= lastSpecialCommand + specialCommandDelayMillis || currentTimeMillis <= lastCommandGlobal + 250) {
            when (commandToValidate) {
                "weather", "misc" -> {
                    BethesdaTwitchIntegration.sendMessage("Too many commands, please wait a bit.")
                    return
                }
            }
        }

        when (commandToValidate) {
            "reload" -> {
                if (command.size == 1) {
                    if (higherPermissions(twitchClient, username)
                    ) {
                        SettingsParser.settingsParser()
                        MiscLogic.startCleanup()
                    }
                } else {
                    BethesdaTwitchIntegration.sendInvalidCommand(username)
                }
                if (command.size == 2) {
                    if (higherPermissions(twitchClient, username)
                    ) {
                        if (MiscLogic.isInteger(command[1])) {
                            val chatters = twitchClient!!.messagingInterface.getChatters(channelToJoin).execute()
                            for (viewer in chatters.allViewers) {
                                PointLogic.modifyUserPoints(command[1].toInt(), viewer)
                            }
                        } else {
                            BethesdaTwitchIntegration.sendInvalidCommand(username)
                        }
                    }
                } else {
                    BethesdaTwitchIntegration.sendInvalidCommand(username)
                }
            }
            "addpointsall" -> if (command.size == 2) {
                if (higherPermissions(twitchClient, username)) {
                    if (MiscLogic.isInteger(command[1])) {
                        val chatters = twitchClient!!.messagingInterface.getChatters(channelToJoin).execute()
                        for (viewer in chatters.allViewers) {
                            PointLogic.modifyUserPoints(command[1].toInt(), viewer)
                        }
                    } else {
                        BethesdaTwitchIntegration.sendInvalidCommand(username)
                    }
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }
            "addpoints" -> if (command.size == 3) {
                if (higherPermissions(twitchClient, username)) {
                    if (PointLogic.userExists(command[1]) && MiscLogic.isInteger(command[2])) {
                        if (username.equals(command[1], ignoreCase = true)) {
                            BethesdaTwitchIntegration.sendMessage("You can't add points to yourself @$username")
                        } else {
                            PointLogic.modifyUserPoints(command[2].toInt(), command[1])
                        }
                    } else {
                        BethesdaTwitchIntegration.sendInvalidCommand(username)
                    }
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }
            "points" -> if (command.size == 1) {
                val tempValue: Int = PointLogic.userPoints(username)
                if (tempValue >= 0) {
                    BethesdaTwitchIntegration.sendMessage("You have $tempValue points @$username")
                } else {
                    BethesdaTwitchIntegration.sendMessage("You have 0 points @$username")
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }
            "givepoints" -> if (command.size == 3) {
                if (PointLogic.userExists(command[1].toLowerCase()) && MiscLogic.isInteger(command[2]) && !command[1].equals(username, ignoreCase = true)) {
                    if (PointLogic.userPoints(username) >= command[2].toInt()) {
                        PointLogic.modifyUserPoints(-command[2].toInt(), username)
                        PointLogic.modifyUserPoints(command[2].toInt(), command[1].toLowerCase())
                        BethesdaTwitchIntegration.sendMessage("@" + username + " gave " + command[2] + " points to @" + command[1].toLowerCase())
                    } else {
                        BethesdaTwitchIntegration.sendMissingPoints(username, command[2].toInt())
                    }
                } else {
                    BethesdaTwitchIntegration.sendInvalidCommand(username)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }
            "purge" -> if (command.size == 1) {
                if (higherPermissions(twitchClient, username)) {
                    if (MiscLogic.isInteger(command[1])) {
                        PointLogic.purgeUsers(command[1].toInt())
                    } else {
                        BethesdaTwitchIntegration.sendInvalidCommand(username)
                    }
                } else {
                    BethesdaTwitchIntegration.sendInvalidCommand(username)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }
            "toggle" -> if (command.size == 1) {
                if (higherPermissions(twitchClient, username)) {
                    systemEnabled = !systemEnabled
                    if (systemEnabled) {
                        BethesdaTwitchIntegration.sendMessage("Enabled Twitch Integration.")
                    } else {
                        BethesdaTwitchIntegration.sendMessage("Disabled Twitch Integration.")
                    }
                } else {
                    BethesdaTwitchIntegration.sendInvalidCommand(username)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }
            "tada" -> if (username.equals("Flenarn_", ignoreCase = true)) {
                BethesdaTwitchIntegration.sendMessage("Hey, look everyone! It's @flenarn_, he developed BTI!")
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }
            "spawn" -> if (command.size == 2) {
                SpawnCommand.handleSpawn(username, command[1])
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }
            "misc" -> {
                if (command.size == 2) {
                    MiscCommand.handleMisc(username, command[1])
                } else {
                    BethesdaTwitchIntegration.sendInvalidCommand(username)
                }
            }
            "give" -> if (command.size == 3) {
                val amount: Int = if (MiscLogic.isInteger(command[2])) {
                    command[2].toInt()
                } else {
                    BethesdaTwitchIntegration.sendInvalidCommand(username)
                    return
                }
                if (amount > 1000) {
                    BethesdaTwitchIntegration.sendMessage("Max amount to spawn is 1000 @$username")
                } else {
                    GiveCommand.handleGive(username, command[1], amount)
                }
            } else if (command.size == 2) {
                GiveCommand.handleGive(username, command[1], 1)
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }
            "randomspawn" -> if (command.size == 1) {
                if (PointLogic.userPoints(username) < randomspawncost.toInt()) {
                    BethesdaTwitchIntegration.sendMissingPoints(username, randomspawncost.toInt())
                } else {
                    RandomSpawnCommand.handleRandomSpawn(username)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }
            "weather" -> if (command.size == 2) {
                WeatherCommand.handleWeather(username, command[1])
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }
            else -> BethesdaTwitchIntegration.sendMessage("Invalid command @$username")
        }
    }

    /*
     * Basic permission check, so command-caller is mod/admin or the streamer.
     */
    private fun higherPermissions(twitchClient: TwitchClient?, username: String): Boolean {
        val chatters = twitchClient!!.messagingInterface.getChatters(channelToJoin).execute()
        return if (chatters.moderators.contains(username) || chatters.admins.contains(username) || username.equals(channelToJoin, ignoreCase = true)) { //Look into deprecation replacement
            true
        } else {
            BethesdaTwitchIntegration.sendInvalidCommand(username)
            false
        }
    }

}