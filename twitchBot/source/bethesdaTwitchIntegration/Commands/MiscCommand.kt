package bethesdaTwitchIntegration.commands

import bethesdaTwitchIntegration.json.CommandValidation
import bethesdaTwitchIntegration.json.CommandValidation.miscCost
import bethesdaTwitchIntegration.json.PointLogic
import bethesdaTwitchIntegration.json.SettingsParser.lastCommandGlobal
import bethesdaTwitchIntegration.json.SettingsParser.lastSpecialCommand
import bethesdaTwitchIntegration.misc.MiscLogic
import org.bethesdaTwitchIntegration.BethesdaTwitchIntegration

object MiscCommand {

    fun handleMisc(username: String, miscCommand: String) {
        if (CommandValidation.validateMisc(miscCommand)) {
            if (PointLogic.userPoints(username) < miscCost) {
                BethesdaTwitchIntegration.sendMissingPoints(username, miscCost)
            } else {
                PointLogic.modifyUserPoints(-miscCost, username)
                MiscLogic.createCommandCall(CommandValidation.miscCommand)
                lastSpecialCommand = System.currentTimeMillis()
                lastCommandGlobal = System.currentTimeMillis()
            }
        }
    }
}