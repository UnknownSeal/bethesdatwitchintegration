package bethesdaTwitchIntegration.commands

import bethesdaTwitchIntegration.json.CommandValidation.allowedInInterior
import bethesdaTwitchIntegration.json.CommandValidation.defaultPlugin
import bethesdaTwitchIntegration.json.CommandValidation.npcAmountToSpawn
import bethesdaTwitchIntegration.json.CommandValidation.npcToSpawn
import bethesdaTwitchIntegration.json.CommandValidation.pluginToLookFor
import bethesdaTwitchIntegration.json.PointLogic
import bethesdaTwitchIntegration.json.SettingsParser.gameJSON
import bethesdaTwitchIntegration.json.SettingsParser.lastCommandGlobal
import bethesdaTwitchIntegration.json.SettingsParser.lastDefaultCommand
import bethesdaTwitchIntegration.json.SettingsParser.randomspawncost
import bethesdaTwitchIntegration.misc.MiscLogic

import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser

import java.io.FileReader
import java.util.Random

object RandomSpawnCommand {

    fun handleRandomSpawn(username : String) {
        val jsonParser = JSONParser()
        try {
            val jsonObject = jsonParser.parse(FileReader("data/$gameJSON/${gameJSON}spawns.json")) as JSONObject
            val jsonToStringArray = jsonObject.keys.toTypedArray() as Array<*>
            val random = Random()
            val randomSpawn = random.nextInt(jsonToStringArray.size)
            val jsonArray = jsonObject[jsonToStringArray[randomSpawn]] as JSONArray
            npcToSpawn = jsonArray[1] as String
            val tempAmount = jsonArray[2] as Long
            npcAmountToSpawn = tempAmount.toInt()
            allowedInInterior = jsonArray[3] as Boolean
            pluginToLookFor = if (jsonArray.size == 5) {
                jsonArray[4] as String
            } else defaultPlugin

            val allowedInInteriorValue: Int = if(allowedInInterior) 1 else 0

            val commandForxSE = "spawn $npcToSpawn $npcAmountToSpawn $allowedInInteriorValue $pluginToLookFor"
            MiscLogic.createCommandCall(commandForxSE)
            lastDefaultCommand = System.currentTimeMillis()
            lastCommandGlobal = System.currentTimeMillis()
            SpawnCommand.lastCommandFired = "$username $randomspawncost"
            PointLogic.modifyUserPoints(-randomspawncost.toInt(), username)
        } catch (exception : Exception) {
            exception.printStackTrace()
        }
    }
}