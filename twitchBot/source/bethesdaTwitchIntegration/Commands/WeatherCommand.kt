package bethesdaTwitchIntegration.commands

import bethesdaTwitchIntegration.json.CommandValidation
import bethesdaTwitchIntegration.json.CommandValidation.pluginToLookFor
import bethesdaTwitchIntegration.json.CommandValidation.weatherCost
import bethesdaTwitchIntegration.json.CommandValidation.weatherID
import bethesdaTwitchIntegration.json.PointLogic
import bethesdaTwitchIntegration.json.SettingsParser.lastCommandGlobal
import bethesdaTwitchIntegration.json.SettingsParser.lastSpecialCommand
import bethesdaTwitchIntegration.misc.MiscLogic
import org.bethesdaTwitchIntegration.BethesdaTwitchIntegration

object WeatherCommand {

    fun handleWeather(username : String, weatherCommand : String) {
        if (CommandValidation.validateWeather(weatherCommand)) {
            if (PointLogic.userPoints(username) < weatherCost) {
                BethesdaTwitchIntegration.sendMissingPoints(username, weatherCost)
            } else {
                PointLogic.modifyUserPoints(-weatherCost, username)
                val commandForxSE = "weather $weatherID $pluginToLookFor"
                MiscLogic.createCommandCall(commandForxSE)
                lastSpecialCommand = System.currentTimeMillis()
                lastCommandGlobal = System.currentTimeMillis()
            }
        } else {
            BethesdaTwitchIntegration.sendInvalidCommand(username)
        }
    }
}