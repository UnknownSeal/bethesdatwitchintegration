# bethesdaTwitchIntegration

Allows for Twitch Integration with the different Bethesda games, currently supports Fallout 4 & Skyrim Legendary Edition, support for Skyrim Special Edition is in development.

### Huge thanks to, as for without you this would never have happened:
**meh321** - for permissions to make use of the consoleUtils (LE) code.

**kinggath** - for Papyrus assistance.

**reverett** - for C++ assistance.

**shavkacagarikia** - for C++ assistance.

**shad0wshayd3** - for C++ assistance.

**reg2k** - for their fantastic RVA implementation.

**JonathanOstrus** - for ideas and help.

**Script Extender Team** - for the script extender.

**Bethesda** - for the fantastic games and opportunity to do this.